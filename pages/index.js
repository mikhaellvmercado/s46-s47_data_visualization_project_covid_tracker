import Head from 'next/head';
import Jumbotron from 'react-bootstrap/Jumbotron';
import toNum from '../helpers/toNum';

export default function Home({globalTotal}) {
  return (
    <div>
      <Head>
        <title>Covid-19 Tracker App</title>
        <link rel="icon" href="/favicon.ico"></link>
      </Head>
      <Jumbotron>
        <h1> Total Covid-19 Cases in the World: <strong>{globalTotal.cases} </strong> </h1>
      </Jumbotron>
    </div>
  )
}

export async function getStaticProps() {
  const res = await fetch("https://coronavirus-monitor.p.rapidapi.com/coronavirus/cases_by_country.php", {
    "method": "GET",
    "headers": {
      "x-rapidapi-host": "coronavirus-monitor.p.rapidapi.com",
      "x-rapidapi-key":"6085b628a5msh12b4765569d1427p1188bbjsnd3c4dc348539"
    }

  })

  const data = await res.json()

   const countriesStats = data.countries_stat

   let total = 0

   countriesStats.forEach(country => {
    total += toNum(country.cases)
   

   })

   const globalTotal = {
     cases: total 
   }

   return {
    props: {
      globalTotal
    }
   }
  }