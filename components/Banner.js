import Jumbotron from 'react-bootstrap/Jumbotron';
import Button from 'react-bootstrap/Button';
import Link from 'next/link'

export function Banner() {
    return(
        <Jumbotron>
            <h1>
            WELCOME USER!
            </h1>
            <p>Deaths: </p>
            <p>Recoveries: </p>
            <p>Critical Cases: </p>
            <Button variant="success">
                <Link href="/">
                    <a> View Countries </a>
                </Link>
            </Button>
        </Jumbotron>
    )
}